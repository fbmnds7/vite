
#|
(require 'asdf)

(asdf:load-asd "~/projects/vite/vite.asd")
(asdf:load-system :vite)
|#

(defun fn (env)
  `(200 nil (,(prin1-to-string env))))

(vite:run (vite:make-app :name "vite"
                         :port 4998
                         :listen "127.0.0.1"
                         :server nil
                         :fn #'fn))



