
(in-package #:vite)


(defparameter *registry* (make-hash-table))
(setf (gethash :init *registry*) nil)

(defstruct app name port server listen fn)


(defun init ()
  (unless (gethash :init *registry*)
    #-quicklisp
    (let ((quicklisp-init #P"/opt/orbi/quicklisp/setup.lisp"))
      (when (probe-file quicklisp-init)
        (load quicklisp-init)))
    (ql:quickload '(clack woo optima alexandria uiop))
    (setf (gethash :init *registry*) t)))

(defun run (app)
  (unless (gethash :init *registry*) (init))
  (if (gethash (app-port app) *registry*)
      (error "port in use")
      (progn
        (setf (app-server app)
              (clack:clackup (lambda (env) (funcall (app-fn app) env))
                             :server :woo
                             :port (app-port app)
                             :address (app-listen app)))
        (setf (gethash (app-port app) *registry*) app))))

(defun stop (port)
  (prog1
      (clack:stop (app-server (gethash port *registry*)))
    (setf (app-server (gethash port *registry*)) nil)))

(defun rerun (port)
  (stop port)
  (let ((app (gethash port *registry*)))
    (when app (run app))))

