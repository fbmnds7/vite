(asdf:defsystem #:vite
  :description "Describe vite here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on
  (#:asdf
   #:quicklisp
   #:clack
   #:woo)
  :components
  ((:file "packages")
   (:file "vite")
   (:module "t"
    :components
    ((:file "load")))))
